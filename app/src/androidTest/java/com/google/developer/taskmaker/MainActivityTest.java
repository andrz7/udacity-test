package com.google.developer.taskmaker;

import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Andreas on 9/9/2017.
 */

@RunWith(JUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class, false, true);

    @Test
    public void clickAddFab_showAddTaskActivity() throws Exception{
        onView(withId(R.id.fab)).perform(click());
        onView(withId(R.id.text_wrapper_description)).check(matches(isDisplayed()));
        onView(withId(R.id.text_input_description)).check(matches(isDisplayed()));
        onView(withId(R.id.switch_priority)).check(matches(isDisplayed()));
        onView(withId(R.id.select_date)).check(matches(isDisplayed()));
    }
}
