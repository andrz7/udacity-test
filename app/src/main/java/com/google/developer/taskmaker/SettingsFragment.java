package com.google.developer.taskmaker;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.google.developer.taskmaker.data.DatabaseContract;
import com.google.developer.taskmaker.data.PreferenceUtils;

public class SettingsFragment extends PreferenceFragment {

    private ListPreference mListPreference;

    Preference.OnPreferenceChangeListener listener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            PreferenceUtils.setStringPref(getActivity(),
                    PreferenceUtils.SORT_SETTING_PREF_KEY, (String) newValue);
            getActivity().getContentResolver().notifyChange(DatabaseContract.CONTENT_URI, null);
            return true;
        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        listener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        mListPreference = (ListPreference) findPreference(getString(R.string.pref_sortBy_key));
        mListPreference.setOnPreferenceChangeListener(listener);
    }
}
