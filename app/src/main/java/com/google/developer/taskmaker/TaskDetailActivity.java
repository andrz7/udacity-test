package com.google.developer.taskmaker;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.developer.taskmaker.data.DatabaseContract;
import com.google.developer.taskmaker.data.Task;
import com.google.developer.taskmaker.data.TaskUpdateService;
import com.google.developer.taskmaker.reminders.AlarmScheduler;
import com.google.developer.taskmaker.views.DatePickerFragment;
import com.google.developer.taskmaker.views.TaskTitleView;

import java.util.Calendar;

import static com.google.developer.taskmaker.data.DatabaseContract.getColumnInt;
import static com.google.developer.taskmaker.data.DatabaseContract.getColumnLong;

public class TaskDetailActivity extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener {

    public static Intent newIntent(Context context, Uri uri) {
        Intent intent = new Intent(context, TaskDetailActivity.class);
        intent.setData(uri);
        return intent;
    }

    private Uri mTaskUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_task);
        //Task must be passed to this activity as a valid provider Uri
        mTaskUri = getIntent().getData();

        //TODO: Display attributes of the provided task in the UI
        bindDataToView(getData(mTaskUri));
    }

    private Task getData(final Uri taskUri) {
        Task task = null;
        Cursor cursor = getContentResolver().query(taskUri, null, null, null, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                task = new Task(cursor);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return task;
    }

    private void bindDataToView(final Task task) {
        if (task != null) {
            TaskTitleView nameView = (TaskTitleView) findViewById(R.id.text_description);
            ImageView priorityView = (ImageView) findViewById(R.id.priority);
            TextView dateView = (TextView) findViewById(R.id.text_date);

            Calendar calendar = Calendar.getInstance();

            nameView.setText(task.description);

            if (task.isComplete) {
                nameView.setState(TaskTitleView.DONE);
            } else {
                if (task.hasDueDate()) {
                    dateView.setText(DateUtils.getRelativeTimeSpanString(task.dueDateMillis));

                    if (calendar.getTimeInMillis() > task.dueDateMillis) {
                        nameView.setState(TaskTitleView.OVERDUE);
                    }
                } else {
                    dateView.setText(getString(R.string.date_empty));
                }
            }

            priorityView.setImageResource(task.isPriority
                    ? R.drawable.ic_priority
                    : R.drawable.ic_not_priority);
        } else {
            Toast.makeText(this, getString(R.string.error_toast_message), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_delete:
                onDeleteClicked();
                return true;
            case R.id.action_reminder:
                onReminderClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        //TODO: Handle date selection from a DatePickerFragment
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        c.set(Calendar.HOUR_OF_DAY, 12);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        Calendar current = Calendar.getInstance();

        if (c.getTimeInMillis() < current.getTimeInMillis()) {
            Toast.makeText(this, getString(R.string.alarm_error_message), Toast.LENGTH_SHORT).show();
        } else {
            AlarmScheduler.scheduleAlarm(this, c.getTimeInMillis(), mTaskUri);
            Toast.makeText(this, getString(R.string.alarm_success_message, c.getTime().toString()),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void onReminderClicked() {
        DatePickerFragment dialogFragment = new DatePickerFragment();
        dialogFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void onDeleteClicked() {
        TaskUpdateService.deleteTask(this, mTaskUri);
        showDialog();
    }

    private void showDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(R.string.dialog_success_title)
                .setMessage(R.string.dialog_success_message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }
}
