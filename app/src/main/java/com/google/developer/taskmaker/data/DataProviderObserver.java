package com.google.developer.taskmaker.data;

import android.database.ContentObserver;
import android.os.Handler;

/**
 * Created by Andreas on 9/9/2017.
 */

public class DataProviderObserver extends ContentObserver {

    public DataProviderObserver(Handler handler) {
        super(handler);
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
    }
}
